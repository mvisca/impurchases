# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class PurchaseItem(scrapy.Item):
    number = scrapy.Field()
    type = scrapy.Field()
    start_date = scrapy.Field()
    end_date = scrapy.Field()
    amount = scrapy.Field()
    currency = scrapy.Field()
    description = scrapy.Field()
    details_url = scrapy.Field()
