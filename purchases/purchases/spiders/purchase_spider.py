from datetime import datetime
from scrapy import Spider, Request
from  scrapy.selector import Selector

from purchases.items import PurchaseItem

class PurchaseSpider(Spider):
    name = "purchase"
    allowed_domains = ["montevideo.gub.uy"]
    start_urls = [
        "http://www.montevideo.gub.uy/asl/sistemas/siab/cartelera.nsf/FinalizadasHTML?OpenView&Count=10000&ResortDescending=1"
    ]

    def _to_date(self, str_date):
        return datetime.strptime(str_date, "%m/%d/%Y")


    def parse(self, response):
        purchases = Selector(response).xpath(
            '/html/body/form/table/tr[1]/td[1]/div/table/tr[@valign="top"]'
        )
        for purchase in purchases:
            purchase_item = PurchaseItem()
            row_data = purchase.xpath(
                'td/font/text()'
            ).extract()

            details_url = purchase.xpath(
                'td/font/a[@title]/@href'
            ).extract()[0]

            if details_url.startswith("/"):
                details_url = f"{self.allowed_domains[0]}{details_url}"

            purchase_item["number"] = row_data[0]
            purchase_item["type"] = row_data[1]
            purchase_item["start_date"] = self._to_date(row_data[2])
            purchase_item["end_date"] = self._to_date(row_data[3])
            currency, amount = row_data[4].split(" ")
            purchase_item["currency"] = currency
            purchase_item["amount"] = round(float(amount.replace(".", "").replace(",", ".")), 2)
            purchase_item["description"] = row_data[5]
            purchase_item["details_url"] = details_url
            yield purchase_item

        next_page = Selector(response).xpath(
            '/html/body/form/table/tr[1]/td[1]/div/a[2]/@href'
        ).extract()[0]

        if next_page:
            yield Request(response.urljoin(next_page), self.parse)
